import React from 'react';

export default function Button({ text, id, functions }) {
  function handleClick(e) {
    const { addMove } = functions;
    const point = e.target.id.split(",");
    addMove(point);
  }

  return (
    <button onClick={handleClick} id={id} className={text}>{text}</button>
  );
}
