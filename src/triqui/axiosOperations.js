import axios from 'axios';

export function getStates() {
  return axios
    .get('http://localhost:3001/states');
}

export function updateState(state) {
  return axios
    .post('http://localhost:3001/states', state);
}
