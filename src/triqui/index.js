import React, { Component } from 'react';
import KeyPad from './KeyPad';
import SweetAlert from 'sweetalert-react';
import { updateState, getStates } from './axiosOperations';
import 'sweetalert/dist/sweetalert.css';

const PLAYER_1_WINS = 1;
const PLAYER_2_WINS = 2;
const TIE = 'TIE';

// Initialize states
const initialState = {
  player: 1,
  result: '',
  board: [['-','-','-'],
          ['-','-','-'],
          ['-','-','-']],
};

class Triqui extends Component {
  constructor(props) {
    super(props);
    this.state = { ...initialState };
    this.addMove = this.addMove.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.addMove = this.addMove.bind(this);
    this.checkGame = this.checkGame.bind(this);

    setInterval(() =>{
      getStates()
        .then(({ data }) => {
          this.setState(data);
        });
    }, 1000);
  }

  handleDelete(e) {
    updateState(initialState)
      .then(() => {
        console.log("Correctly sent!");
      });
  }

  addMove(point) {
    let { player, board } = this.state;
    if (board[point[0]][point[1]] === '-') {
      const symbol = player === 1 ? 'X' : 'O';

      board[point[0]][point[1]] = symbol;
      this.setState({
        board,
      }, () => {
        if (this.checkGame(board)) {
          this.setState({
            result: player,
          });
        } else if (this.checkTie(board)) {
          this.setState({
            result: TIE,
          });
        }

        updateState(this.state)
          .then(() => {
            console.log("Correctly sent!");
          });
      });
    }
  }

  checkGame(board) {
    const winningPossibilities = [
      [[0, 0], [0, 1], [0, 2]],
      [[1, 0], [1, 1], [1, 2]],
      [[2, 0], [2, 1], [2, 2]],

      [[0, 0], [1, 0], [2, 0]],
      [[0, 1], [1, 1], [2, 1]],
      [[0, 2], [1, 2], [2, 2]],

      [[0, 0], [1, 1], [2, 2]],
      [[2, 2], [1, 1], [0, 0]],
    ];

    return winningPossibilities.some(possibility => {
      const allX = possibility.every(point => board[point[0]][point[1]] === 'X');
      const allO = possibility.every(point => board[point[0]][point[1]] === 'O');

      return allX || allO;
    });
  }

  checkTie(board) {
    board.every(row => row.every(cell => cell === 'X' || cell === 'O'));
  }

  render() {
    const { board, result } = this.state;
    const functions = {
      addMove: this.addMove,
    };

    return (
      <div>
        <SweetAlert
          show={result === TIE}
          title="Game over"
          text="Game over"
          onConfirm={() => this.setState({ result: '' })}
        />
        <SweetAlert
          show={result === PLAYER_1_WINS || result === PLAYER_2_WINS}
          title={`The winner is ${result}`}
          text={`The winner is ${result}`}
          onConfirm={() => this.setState({ ...initialState })}
        />

        <button onClick={this.handleDelete}>Borrar</button>
        <KeyPad functions={functions} board={board}/>
      </div>
    );
  }
}

export default Triqui;
