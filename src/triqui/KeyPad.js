import React from 'react';
import Button from './Button';

export default function KeyPad({ functions, board }) {
  return (
    <div>
      {board.map((row, index) => (
        <div key={index}>
          {row.map((key, col) => (<Button key={`${index}${col}`} id={[index, col]} text={board[index][col]} functions={functions} />))}
        </div>
      ))}
    </div>
  );
}
